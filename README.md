# UK D8 Dev Container

This is a full Drupal development environment based on Docker. It's intended to simplify developing a single site that uses the UK Drupal 8 Installation Profile, or to develop the installation profile itself.

**Note:** By default, the Docker containers in this project will map your host system `~/.ssh` directory, giving them access to your user's ssh keys. These containers are meant for local development only, and should not be pushed to another system unless this mapping is disabled. This configuration can be modified in the project's `docker-compose.yml` file if desired.

## Managing files using VS Code Remote Containers
For best performance, you can manage your code within a Docker volume, and use VS Code to browse your workspace files. Your workspace files will not be available from your host operating system.

**Note:** When using Remote Containers, VS Code will keep your workspace files on a docker volume that is named after the folder of the dev container. If you recreate a new dev container project with the same folder name, VS Code will mount the existing docker volume. You can manage docker volumes from within VS Code, or any other docker administration tool.



This setup requires:
* Docker Desktop 2.0+ (for macOS or Windows 10 Pro/Enterprise)
* VS Code
* VS Code Remote - Containers extension


### Setup

***Start the docker environment with VS Code***
1. Clone this repository into your project folder. The name of the folder containing `.devcontainer` will be used to name the dev container volume that holds your workspace files.

```
$ cd ~/my-project
$ git clone git@gitlab.com:uky-web/university-web-platform/drupal-8/devcontainer.git .
```


2. Optionally include a SQL dump to be imported by naming it `drupal.sql` and placing it in `docker/mysql`. When the MySQL container is initialized, it will import your dump file rather than create an empty database.

3. Open your project folder in VS Code, selecting to 'Open in Container' when prompted, or enter `Remote-Containers; Reopen in container` in the VS Code command palette. 

VS Code will build and start the necessary Docker containers, and display an empty `site` directory in the file explorer. 

4. Add your project files to the `site` folder by dropping them into the file explorer, or cloning in git via the terminal. Opening a new terminal in VS Code will start a shell session within the dev container.

You can find the dev container's port by clicking on the Remote Explorer menu on the left, and viewing Forwarded Ports.


### Managing files on your Host filesystem
Instead of managing your project code within a Docker volume and accessing it through VS Code, you can also map your code from your host filesystem into Docker. Note that this may create performance issues.
